getpeaker
Путь к этому репозиторию: https://gitlab.com/rybakoviztomska/test_work_getpeaker

getpeaker.с и getpeaker.h - алгоритмы поиска локальных максимумов превышающих на 10 дБ средний уровень:
int16_t getPeak(int16_t * RD, int16_t r_size, int16_t d_size, uint32_t * outIndexRD) - 2D реализация.
int16_t getPeak3D(uint16_t * RDX, int16_t r_size, int16_t d_size, int16_t x_size, uint64_t * outIndexRDX) - 3D реализация.

getpeakerC6000.с и getpeakerC6000.h - алгоритмы поиска локальных максимумов превышающих на 10 дБ средний уровень c
с оптимизацией под TMC320C6000 с результатами и описанием конвееров после компиляции:
int16_t getPeak(int16_t * RD, int16_t r_size, int16_t d_size, uint32_t * outIndexRD) - 2D реализация.
Дополнительно там предложения оптимизации по скорости исполнения на DSP TMS320C6000.
Сбрка компилятором осуществлялась с параметрами: -O2 --opt_for_speed=4

tests.cpp - юнит тесты:
void tests () - запуск всех тестов.
Ожидаемый вывод:
UNIT-TEST: test_1
Result test_1: SUCCESS

UNIT-TEST: test_2
Result test_2: SUCCESS

UNIT-TEST: test_3
Result test_3: SUCCESS

UNIT-TEST: test_RealData_1
Result test_RealData_1: SUCCESS

UNIT-TEST: test_treshold_1
Result test_treshold_1: SUCCESS

UNIT-TEST: test3D_1
Result test3D_1: SUCCESS

UNIT-TEST: test3D_RealData_1
Result test3D_RealData_1: SUCCESS

UNIT-TEST: test_compare_treshold
Result test_compare_treshold: SUCCESS

UNIT-TEST: test_compare_RealData
Result test_compare_RealData: SUCCESS


FINAL RESULT: SUCCESS

test.data - данные в виде таблицы из surface.bin совместимые для добавления непосредственно в Си файлы,
используется для юнит тестов.




